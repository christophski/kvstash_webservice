from django.db import models
from django.core import validators


class Client(models.Model):

    """Models a client which is using this API

    This class models clients that have registered to use this api. It is referenced by the KVPair model which
    uses a Client instance to make it unique to and exclusively accessibly by that Client.

    """

    # The token is a unique value identifying this Client.
    token = models.CharField(
        max_length=48
        , unique=True
        , validators=[validators.MinLengthValidator(1)]
    )


class KVPair(models.Model):

    """Models a key-value pair that is used by clients to store data.

    This class models key-value pairs that belong to a client. Every KVPair object must belong to exactly one valid
    Client instance and access should only be permitted for authorized Clients.

    """

    #Every KVPair belongs to a client and is only accessible by that client. A foreign key relation allows keys to
    #be retrieved from a Client object and for KVPairs to determine their parent client.
    client = models.ForeignKey(Client, related_name="pairs")

    #key identifies this key/value pair
    key = models.CharField(
        max_length=20
        , blank=False
        , validators=[validators.MinLengthValidator(1)]
    )

    #the value of this key/value pair
    value = models.CharField(
        max_length=100
        , null=True
    )

    class Meta:
        #keys must be unique per client but not for the entire table.
        unique_together = ("key", "client")
