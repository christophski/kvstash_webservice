import json
from operator import itemgetter
import random

from django.test import TestCase
import unittest

from .models import Client, KVPair
from . import security
import string


class SecurityUnitTests(unittest.TestCase):
    """ Defines unit tests for the api.security module """

    def test_generate_api_key(self):
        key = security.generate_api_key()
        self.assertEquals(len(key),48)

        #make sure that the output contains only base64 characters and that it is url safe
        self.assertRegex(key.decode('utf-8'), r"^[A-Za-z0-9]+$")


class RegisterViewTestCase(TestCase):
    """ This class provides test methods for all endpoints of the Register(View) class (api.views.Register)"""

    def test_get(self):
        """This method is used to test GET access to the Register view/class

        Conditions tested:

            - Creation of new api token

        """

        resp = self.client.get('/api/register/')
        self.assertEqual(resp.status_code, 201)

        #extract the api token from the response
        token = json.loads(resp.content.decode('utf-8')).get('token')

        #make sure that a Client has been created using the genereated api key
        client = Client.objects.get(token=token)


class DataViewTestCase(TestCase):
    """ This class provides test methods for all endpoints of the Data(View) class (api.views.Data).

    Current method test coverage:

    get(request)
    post(request)

    """

    def test_get(self):
        """ This method is used to test GET access of the Data view/class

        Conditions tested:

            - Invalid/non existent api token used
            - No api token included in request
            - Returning all k/v pairs when 'key' parameter is omitted
            - Returning correct k/v pair when existing key is supplied
            - Supplied 'key' parameter doesn't map to k/v pair for specified client

        """

        req_root = '/api/data/'

        # Setup client to work with.
        client = self.__genTestClient()

        token = "?token=" + client.token.decode('utf-8')

        # Add sample data to the client.
        client.pairs.add(KVPair(key='hello', value='world'))
        client.pairs.add(KVPair(key='goodbye', value='Jim'))
        client.pairs.add(KVPair(key='love', value='goats'))

        #######################################################################################################
        # Invalid request, api token doesn't exists, should yield a response with status 404
        #######################################################################################################
        resp = self.client.get(req_root + "?token=this_is_an_invalid_token")
        self.assertEqual(resp.status_code, 404)


        #######################################################################################################
        # Invalid request, api token not provided, should yield a response with status 400
        #######################################################################################################
        resp = self.client.get(req_root)
        self.assertEqual(resp.status_code, 400)


        #######################################################################################################
        # Legitimate request, should return all 3 k/v pairs with status 200
        #######################################################################################################
        resp = self.client.get(req_root + token)

        #get kvpairs sorted by key from response
        pairs = sorted(json.loads(resp.content.decode('utf-8')), key=itemgetter('key'))

        self.assertEqual(resp.status_code, 200);
        self.assertEqual(len(pairs), 3)

        self.assertEqual(pairs[0].get('key'), 'goodbye')
        self.assertEqual(pairs[0].get('value'), 'Jim')

        self.assertEqual(pairs[1].get('key'), 'hello')
        self.assertEqual(pairs[1].get('value'), 'world')

        self.assertEqual(pairs[2].get('key'), 'love')
        self.assertEqual(pairs[2].get('value'), 'goats')


        #######################################################################################################
        # Legitimate request with existing key specified, should yield response with status 200 and content of
        # key/value
        #######################################################################################################
        resp = self.client.get(req_root + token + "&key=love")
        pairs = json.loads(resp.content.decode('utf-8'));

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(pairs), 1)
        self.assertEqual(pairs[0].get('key'), 'love')
        self.assertEqual(pairs[0].get('value'), 'goats')


        #######################################################################################################
        # Legitimate request but with non existent key, should yield response with status 404
        #######################################################################################################

        resp = self.client.get(req_root + token + "&key=this_key_does_not_exist")
        self.assertEqual(resp.status_code, 404)


    def test_post(self):
        """ This method is used to test POST access of the Data view/class

         Conditions tested:

            - Invalid/non existent api token used
            - No api token included in request
            - Valid key/value parameters used to create a new KV Pair
            - Valid key/value parameters used, where key already exists and the key's value is updated
            - No 'key' parameter sent with request
            - No 'value' parameter sent with request
            - 'key' parameter supplied, but too long
            - 'key' parameter supplied, but too short
            - 'value' parameter supplied, but too long

        """

        req_root = "/api/data/"

        #get client to work with
        client = self.__genTestClient()

        token = client.token.decode('utf-8')


        #######################################################################################################
        # Invalid request, api token doesn't exists, should yield a response with status 404
        #######################################################################################################
        resp = self.client.post(req_root, {"token": "this_is_an_invalid_token", "key": "somekey", "value": "somevalue"})
        self.assertEqual(resp.status_code, 404)


        #######################################################################################################
        # Invalid request, api token not provided, should yield a response with status 400
        #######################################################################################################
        resp = self.client.post(req_root, {"key": "somekey", "value": "somevalue"})
        self.assertEqual(resp.status_code, 400)


        #######################################################################################################
        # Legitimate request, should create a KVPair for the given client with the specified key and value.
        # The response should have a status of 201
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "key": "testkey", "value": "testvalue"})

        self.assertEqual(resp.status_code, 201)
        self.assertEqual(client.pairs.get(key='testkey').value, 'testvalue')


        #######################################################################################################
        # Legitimate request, should update the KVPair with key=testkey, yielding a response with status 200
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "key": "testkey", "value": "new_test_value"})

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(client.pairs.get(key='testkey').value, 'new_test_value')


        #######################################################################################################
        # Invalid request, key omitted, should yield response with status 400
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "value": "somevalue"})
        self.assertEqual(resp.status_code, 400)


        #######################################################################################################
        # Invalid request, value omitted, should yield response with status 400
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "key": "somekey"})
        self.assertEqual(resp.status_code, 400)


        #######################################################################################################
        # Invalid request, key too long (max 20), should yield response with status 400
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "key": self.__getRandString(21), "value": "testvalue"})
        self.assertEqual(resp.status_code, 400)

        #######################################################################################################
        # Invalid request, key too short (min 1), should yield response with status 400
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "key": '', "value": "testvalue"})
        self.assertEqual(resp.status_code, 400)


        #######################################################################################################
        # Invalid request, value too long (max 100) , should yield response with status 400
        #######################################################################################################
        resp = self.client.post(req_root, {"token": token, "key": "testkey", "value": self.__getRandString(101)})
        self.assertEqual(resp.status_code, 400)


    def __genTestClient(self):
        """ This method simply creates and saves (to the temporary database) a Client model instance for use in testing
        """
        c = Client(token=security.generate_api_key())
        c.save()
        return c

    def __getRandString(self, len):
        """ Generates a random string of a given length """
        return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(len)])
