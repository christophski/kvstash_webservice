import random
import hashlib
import base64
import uuid


def generate_api_key():
    return base64.urlsafe_b64encode(bytes(str(uuid.uuid4()),'UTF-8'))