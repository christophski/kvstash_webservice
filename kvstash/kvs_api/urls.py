from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = patterns(
    '',
    url(r'^register/', views.Register.as_view()),
    url(r'^data/', views.Data.as_view())
    )

urlpatterns = format_suffix_patterns(urlpatterns)