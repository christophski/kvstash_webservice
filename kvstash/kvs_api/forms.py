from django.forms import ModelForm
from .models import Client, KVPair

class ClientForm(ModelForm):
    class Meta:
        model = Client
        fields = ['token']


class KVPairForm(ModelForm):
    class Meta:
        model = KVPair
        fields = ['key', 'value']
