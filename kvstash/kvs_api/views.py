from django.http import HttpResponse
from django.views.generic import View
from django.forms import models

import json

from .models import Client, KVPair
from . import security
from .forms import ClientForm, KVPairForm


def simple_error(message):
    return json.dumps({'error': message})


class Register(View):
    """ This View class allows third party clients to generate an api token, that they can use in subsequent request
    in order to access key/value pairs stored against that token

    """

    def get(self, request):
        c = Client()
        c.token = security.generate_api_key()
        form = ClientForm({'token': c.token})
        if form.is_valid():
            c.save()
            return HttpResponse(json.dumps({'token': form.instance.token}), content_type="application/json", status=201)
        return HttpResponse(status=500)


class Data(View):
    def post(self, request):

        #input parameters
        t = request.POST.get("token", None)
        k = request.POST.get("key", None)
        v = request.POST.get("value", None)

        if t is None:
            return HttpResponse(simple_error("'token' request parameter is mandatory"), status=400)

        if k is None:
            return HttpResponse(simple_error("'key' request parameter is mandatory"), status=400)

        if v is None:
            return HttpResponse(simple_error("'value' request parameter is mandatory"), status=400)

        try:
            c = Client.objects.get(token=t)
        except Client.DoesNotExist:
            return HttpResponse(simple_error("Invalid api token"), status=404)

        #get a key value pair to work with (either existing or a new one)
        try:
            kvp = c.pairs.get(key=k)
            kvp.value = v
            status_code = 200
        except KVPair.DoesNotExist:
            kvp = KVPair(client=c, key=k, value=v)
            status_code = 201

        form = KVPairForm(data=models.model_to_dict(kvp), instance=kvp)

        if form.is_valid():
            try:
                form.save()
            except Exception as e:
                return HttpResponse(status=500)
            return HttpResponse(status=status_code)
        else:
            return HttpResponse(simple_error(form.errors), status=400)


    def get(self, request):
        t = request.GET.get("token", None)
        k = request.GET.get("key", None)

        if t is None:
            return HttpResponse(simple_error("'token' request parameter is mandatory"), status=400)

        try:
            c = Client.objects.get(token=t)
        except Client.DoesNotExist:
            return HttpResponse(simple_error("Invalid api token"), status=404)

        if k is not None:
            try:
                kvp = KVPair.objects.get(client=c, key=k)
            except KVPair.DoesNotExist:
                return HttpResponse([], status=404)
            return HttpResponse(json.dumps([{"key": kvp.key, "value": kvp.value}]), status=200)
        else:
            kvp_list = []
            for p in KVPair.objects.filter(client=c).values():
                kvp_list.append({"key": p["key"], "value": p["value"]})
            return HttpResponse(json.dumps(kvp_list), status=200)